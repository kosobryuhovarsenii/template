// Подключение плагинов
const {
    src,
    dest,
    parallel,
    series,
    watch
} = require("gulp");
const browserSync = require("browser-sync").create();
const del = require("del");
const gulpif = require("gulp-if");
const cached = require("gulp-cached");
const argv = require("yargs").argv;
const plumber = require("gulp-plumber");
const validator = require("gulp-w3c-html-validator");
const concat = require("gulp-concat");
const uglify = require("gulp-uglify");
const scss = require("gulp-sass");
const autoprefixer = require("gulp-autoprefixer");
const sourcemaps = require("gulp-sourcemaps");
const cleanCSS = require("gulp-clean-css");
const imagemin = require("gulp-imagemin");
const imgCompress = require('imagemin-jpeg-recompress');

const baseDir = "dist";

const stylesFile = "app/assets/scss/main.scss";
const fontsFile = "app/assets/fonts/**/*.*";
const imagesFile = "app/assets/img/**/*.{gif,png,jpg,svg,webp}";
const scriptsFile = [
    "app/assets/js/*.js",
];
const libsFile = [
    // "node_modules/svg4everybody/public/svg4everybody.min.js",
    "app/assets/js/libs/*.js",
];

// Прода
const stylesDir = "dist/assets/css";
const fontsDir = "dist/assets/fonts";
const imagesDir = "dist/assets/img/";
const scriptsDir = "dist/assets/js/";
const libsDir = "dist/assets/libs/";


function startwatch() {
    watch(["app/*.html"], html);
    watch(["app/assets/scss/**/*.scss"], styles);
    watch(["app/assets/fonts/**/*.*"], fonts);
    watch(["app/assets/js/*.js"], scripts);
    watch(["app/assets/libs/*.js"], libsjs);
    watch(["app/assets/img/**/*.{gif,png,jpg,svg,webp}"], imageMinify);
}

function clean(cb) {
    return del("dist").then(() => {
        cb();
    });
}

exports.browsersync = browsersync;
exports.html = html;
exports.styles = styles;
exports.fonts = fonts;
exports.scripts = scripts;
exports.libsjs = libsjs;
exports.imageMinify = imageMinify;
exports.clean = clean;
exports.startwatch = startwatch;
exports.default = series(
    clean,
    parallel(html, styles, fonts, scripts, libsjs, imageMinify),
    parallel(browsersync, startwatch)
);


// Автоматическая перезагрузка браузера + лок.хост
function browsersync() {
    browserSync.init({
        server: {
            baseDir: `${baseDir}`
        },
        notify: false,
        online: true,
    });
}

function html() {
    return src("app/*.html")
        .pipe(cached("linting"))
        .pipe(gulpif(argv.prod, validator()))
        .pipe(dest("dist"))
        .pipe(browserSync.stream());
}

function styles() {
    return src(`${stylesFile}`)
        .pipe(plumber())
        .pipe(gulpif(!argv.prod, sourcemaps.init()))
        .pipe(scss())
        .pipe(concat("main.min.css"))
        .pipe(
            gulpif(
                argv.prod,
                autoprefixer({
                    overrideBrowserslist: ["last 10 version"],
                    grid: true
                })
            )
        )
        .pipe(
            gulpif(
                argv.prod,
                cleanCSS({
                        level: {
                            2: {
                                all: true,
                                specialComments: 0,
                            },
                        },
                        compatibility: "*",
                        debug: true,
                    },
                    (details) => {
                        console.log(`${details.name}: ${details.stats.originalSize}`);
                        console.log(`${details.name}: ${details.stats.minifiedSize}`);
                    }
                )
            )
        )
        .pipe(gulpif(!argv.prod, sourcemaps.write(".")))
        .pipe(dest(`${stylesDir}`))
        .pipe(browserSync.stream());
}

function fonts() {
    return src(fontsFile).pipe(dest(fontsDir));
}

function scripts() {
    return (
        src(scriptsFile)
        .pipe(concat("app.min.js"))
        .pipe(uglify())
        .pipe(dest(scriptsDir))
        .pipe(browserSync.stream())
    );
}

function libsjs() {
    return src(libsFile)
        .pipe(concat("libs.min.js"))
        .pipe(uglify())
        .pipe(dest(libsDir))
        .pipe(browserSync.stream());
}

function imageMinify() {
    return (
        src(imagesFile)
        .pipe(
            gulpif(
                argv.prod,
                imagemin([
                    imgCompress({
                        loops: 4,
                        min: 70,
                        max: 80,
                        quality: 'high'
                    }),
                    imagemin.gifsicle(),
                    imagemin.optipng(),
                    imagemin.svgo()
                ])
            )
        )
        .pipe(dest(imagesDir))
        .pipe(browserSync.stream())
    )
}